<?php
class CCTP_rest_settings
{
    /**
     * Registers our plugin with WordPress.
     */
    public static function register()
    {
        $restCls = new self();
        // register hooks
        add_action('rest_api_init', array($restCls, 'cctp_register_posts_meta_field'));      
        add_action('init', array($restCls, 'cctp_add_restrout'));   
        // add_action('init', array($restCls, 'cctp_historical_chart_data'));   

    }

    /**
     * Constructor.
     */ 
    public function __construct()
    {
        // Setup your plugin object here
    }

    //Register Post Meta Field
    public function cctp_register_posts_meta_field()
    {
        $field = 'my_field';
        register_rest_field(
            'crypto_portfolio',
            $field,
            array(
                'get_callback' => function ($object) use ($field) {
                    // Get field as single value from post meta.
                    return get_post_meta($object['id'], $field, true);
                },
                'update_callback' => function ($value, $object) use ($field) {
                    // Update the field/meta value.
                    update_post_meta($object->ID, $field, $value);
                },
            ));
        $portfolio_field = 'total_portfolio';
        register_rest_field(
            'crypto_portfolio',
            $portfolio_field,
            array(
                'get_callback' => function ($object) use ($portfolio_field) {
                    // Get field as single value from post meta.
                    return get_post_meta($object['id'], $portfolio_field, true);
                },
                'update_callback' => function ($value, $object) use ($portfolio_field) {
                    // Update the field/meta value.
                    update_post_meta($object->ID, $portfolio_field, $value);
                },
            ));
        $portfolio_best_worst = 'portfolio_best_worst';
        register_rest_field(
            'crypto_portfolio',
            $portfolio_best_worst,
            array(
                'get_callback' => function ($object) use ($portfolio_best_worst) {
                    // Get field as single value from post meta.
                    return get_post_meta($object['id'], $portfolio_best_worst, true);
                },
                'update_callback' => function ($value, $object) use ($portfolio_best_worst) {
                    // Update the field/meta value.
                    update_post_meta($object->ID, $portfolio_best_worst, $value);
                },
            ));
        $portfolio_field = 'portfolio_assets';
        register_rest_field(
            'crypto_portfolio',
            $portfolio_field,
            array(
                'get_callback' => function ($object) use ($portfolio_field) {
                    // Get field as single value from post meta.
                    return get_post_meta($object['id'], $portfolio_field, true);
                },
                'update_callback' => function ($value, $object) use ($portfolio_field) {
                    // Update the field/meta value.
                    update_post_meta($object->ID, $portfolio_field, $value);
                },
            ));
        $portfolio_field = 'portfolio_fiat_currency';
        register_rest_field(
            'crypto_portfolio',
            $portfolio_field,
            array(
                'get_callback' => function ($object) use ($portfolio_field) {
                    // Get field as single value from post meta.
                    return get_post_meta($object['id'], $portfolio_field, true);
                },
                'update_callback' => function ($value, $object) use ($portfolio_field) {
                    // Update the field/meta value.
                    update_post_meta($object->ID, $portfolio_field, $value);
                },
            ));
        $portfolio_field = 'portfolio_color_scheme';
        register_rest_field(
            'crypto_portfolio',
            $portfolio_field,
            array(
                'get_callback' => function ($object) use ($portfolio_field) {
                    // Get field as single value from post meta.
                    return get_post_meta($object['id'], $portfolio_field, true);
                },
                'update_callback' => function ($value, $object) use ($portfolio_field) {
                    // Update the field/meta value.
                    update_post_meta($object->ID, $portfolio_field, $value);
                },
            ));
    }

    public function cctp_add_restrout()
    {
        // rest api endpoint for sitemap generation
        add_action('rest_api_init', function () {
            register_rest_route('coin-market-cap/v1', 'cmc_get_top_100_coins', array(
                'methods' => 'GET',
                'callback' => array($this, 'cmc_get_top_100_coins'),
                'permission_callback' => '__return_true',
            ));
            register_rest_route('coin-market-cap/v1', 'cctp_historical_chart_data', array(
                'methods' => 'POST',
                'callback' => array($this, 'cctp_historical_chart_data'),
                'permission_callback' => '__return_true',
            ));
            register_rest_route('coin-market-cap/v1', 'cmc_get_fiat_price', array(
                'methods' => 'GET',
                'callback' => array($this, 'cmc_get_fiat_price'),
                'permission_callback' => '__return_true',
            ));
            register_rest_route('coin-market-cap/v1', 'cctp_save_fiat_currency', array(
                'methods' => 'POST',
                'callback' => array($this, 'cctp_save_fiat_currency'),
                'permission_callback' => '__return_true',
            ));
        });
    }

    //Fiat Currency Endpoint
    public function cmc_get_fiat_price()
    {
        $response['status'] = "success";
        $response['data'] = cmc_usd_conversions("all");
        echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        die();
    }

    //100 Coins Array  Endpoint
    public function cmc_get_top_100_coins()
    {
        $cmcDB = new CMC_Coins;
        $coindata = $cmcDB->get_top_changers_coins(array("number" => "100"));
        $response['status'] = "success";
        $response['data'] = $coindata;
        echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        die();
    }

    // Fetch or Save Historical Data
    public function cctp_get_rest_historical_data($coin_id,$days)
    {
        $meta_tbl = new CMC_Coins_historical();
        $coin_symbol = $coin_id;
        $no_days = $days;
        if($no_days == 2){
        $historical_data1 = get_transient("cmc-" . $coin_symbol . '-history-data-24H');
        $transient =  $historical_data1 ;
        }
        else{
        $historical_data365 = get_transient("cmc-" . $coin_symbol . '-history-data-365');
        $transient =  $historical_data365;
        }
        $response = (empty($transient)) ? cmc_historical_chart_json($coin_symbol, $no_days) : $meta_tbl->cmc_get_historical_data($coin_symbol, $no_days);
        $data = json_encode($response);
        return $data;
    }

    public function cctp_save_fiat_currency($response)
    {
        $data = $response->get_params();

        if (isset($data["portfolio_fiat_currency"]) && !empty($data["portfolio_fiat_currency"])) {
            update_option("cctp_get_currency", json_decode($data["portfolio_fiat_currency"]));

        }
        die();
    }

    //Fetch Chart Data Endpoint
    public function cctp_historical_chart_data($data){
        $id_data = $data->get_params();
        $id=$id_data["portfolio_id"];
        $data_1d =  $this->cctp_historic_7d($id);
        $data_1y =  $this->cctp_historic_1y($id);
        $response['status'] = "success";
        $response['data'] =  ['data_7d'=>$data_1d,'data_1y'=>$data_1y];
        echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        die();
    }

    // Fetch One Hour Historical data
    public function cctp_historic_7d($id){
        $data = get_post_meta($id, "my_field");
        $assets = get_post_meta($id, "portfolio_assets");
        $coin_historic_data = [];
        $final_data=[];
        foreach($assets[0] as $asset){
            $coin_quantity = 0;
            $data_7="";
            $filter_7d=[];
            $coin_transaction = [];
            foreach($data[0] as $coin){
                if($asset == $coin['coin']){
                    array_push($coin_transaction,$coin);
                }
            }
            usort($coin_transaction,array($this,'timecomp'));
            $request = $this->cctp_get_rest_historical_data($asset,2); 
            $data_7=json_decode($request);
            $filter_7d = json_decode($request);
            $first_transaction = strtotime($coin_transaction[0]['transaction_date']);
            $closet_first_transaction = $this->getClosest($first_transaction,$data_7);
            foreach($coin_transaction as $transaction){
                $transaction_timestamp = strtotime($transaction['transaction_date']);
                $closest_transaction = $this->getClosest($transaction_timestamp,$data_7);
                foreach($data_7 as $key => $single_data){
                    if($single_data->date == $closest_transaction){
                        if($transaction['transaction_type'] == 'buy'){
                            $coin_quantity = $coin_quantity + $transaction['quantity'];
                        }else{
                            $coin_quantity =$coin_quantity - $transaction['quantity'];
                        }
                    }
                    if($closet_first_transaction > $single_data->date ){
                        $filter_7d[$key]->value = $single_data->value * 0;
                    }
                    if($closest_transaction  <= $single_data->date ){
                        $filter_7d[$key]->value = $single_data->value * $coin_quantity;
                    }
                    
                }
                
            }
            if(empty($coin_historic_data)){
                $coin_historic_data = $filter_7d;
            }
              else{
                foreach($filter_7d as $key=>$single_point)
                {
                    $coin_historic_data[$key]->value += $single_point->value;
                }
            }
                }
        return $coin_historic_data;
      
    }

    //Fetch One Year Historical Data
    public function cctp_historic_1y($id){
        $data = get_post_meta($id, "my_field");
        $filter_data = $data[0];
        $assets = get_post_meta($id, "portfolio_assets");
        $coin_historic_data = [];
        $final_data=[];
        foreach($assets[0] as $asset){
            $coin_quantity = 0;
            $coin_transaction = [];
            foreach($filter_data as $key=>$coin){
                if($asset === $coin['coin']){
                    array_push($coin_transaction,$coin);
                }
            }
            usort($coin_transaction,array($this,'timecomp'));
            $request = $this->cctp_get_rest_historical_data($asset,365);
            $data_1y=json_decode($request);
            $data=json_decode($request);
            $data_1y_slice = array_slice( $data_1y , -364);
            $filter_1y =array_slice( $data , -364);
            $first_transaction = strtotime($coin_transaction[0]['transaction_date']);
            $closet_first_transaction = $this->getClosest($first_transaction,$data_1y_slice);
            foreach($coin_transaction as $transaction){
                $transaction_timestamp = strtotime($transaction['transaction_date']);
                $closest_transaction = $this->getClosest($transaction_timestamp,$data_1y_slice);
                foreach($data_1y_slice as $key => $single_data){
                    if($single_data->date == $closest_transaction){
                        if($transaction['transaction_type'] == 'buy'){
                            $coin_quantity = $coin_quantity + $transaction['quantity'];
                        }else{
                            $coin_quantity =$coin_quantity - $transaction['quantity'];
                        }
                    }
                    if($closet_first_transaction > $single_data->date ){
                        $filter_1y[$key]->value = $single_data->value * 0;
                    }
                    if($closest_transaction  <= $single_data->date ){      
                        $filter_1y[$key]->value = $single_data->value * $coin_quantity;     
                    }
                    
                }
                
            }
            
           
            if(empty($coin_historic_data)){
                $coin_historic_data = $filter_1y;
            }
              else{
                foreach($filter_1y as $key=>$single_point)
                {
                    $coin_historic_data[$key]->value += $single_point->value;
                }
        }
        }
        return $coin_historic_data;
      
    }
    
    // Set Transaction in Ascending Order
    function timecomp($a,$b)
    { 
        return strtotime($a['transaction_date'])-strtotime($b['transaction_date']);
    }

    //Compare Timestamp and Fetch Closest One 
    function getClosest($search, $ts) {
        if(empty($ts)){
            return 0;
        }
        $length = count($ts)-1;
        $closest = $search;
        $first_date = substr($ts[0]->date,0,10);
        $last_date = substr($ts[$length]->date,0,10);
        if ($first_date > $closest) {
            $closest = $ts[0]->date;
        } 
        for ($i = 0; $i < $length ; $i++) {
            if (substr($ts[$i]->date,0,10) < $search && substr($ts[$i+1]->date,0,10) > $search ) {
                $closest = $ts[$i]->date;
                
            }
            else if(substr($ts[$length]->date,0,10) < $search){
                $closest = $ts[$length]->date;
            } 
        }
        if($last_date < $search){
            $closest = 9999999999999;
        }
             
        return $closest;
    }

    // Formatting Number
     function cctp_format_number($n)
    {

        if ($n >= 1) {
            return $formatted = number_format($n, 2, '.', ',');
        } else if ($n >= 0.50 && $n < 25) {
            return $formatted = number_format($n, 3, '.', ',');
        } else if ($n >= 0.01 && $n < 0.5) {
            return $formatted = number_format($n, 4, '.', ',');
        } else if ($n >= 0.0001 && $n < 0.01) {
            return $formatted = number_format($n, 5, '.', ',');
        } else if ($n >= 0.00001 && $n < 0.0001) {
            return $formatted = number_format($n, 6, '.', ',');
        } else if ($n >= 0.000001 && $n < 0.00001) {
            return $formatted = number_format($n, 8, '.', ',');
        } else if ($n <= -0.01) {
            return $formatted = number_format($n, 2, '.', ',');
        } else if ($n <= -0.001) {
            return $formatted = number_format($n, 3, '.', ',');
        } else if ($n <= -0.00001) {
            return $formatted = number_format($n, 5, '.', ',');
        } else if ($n <= 0) {
            return $formatted = number_format($n, 2, '.', ',');
        } else {
            return $formatted = number_format($n, 8, '.', ',');
        }
    }
   

}
CCTP_rest_settings::register();
