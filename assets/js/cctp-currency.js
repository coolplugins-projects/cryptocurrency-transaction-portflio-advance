
jQuery(document).ready(function name($) {
    var domain = data_object.domain_url;
    var storagename = 'cmc-selected-currency-' + domain;
    $(".cmc_conversions").on("change", function () {
        var selected_curr = $('option:selected', this).val();

        var currencySymbol = $('option:selected', this).data('currency-symbol');
        var currencyRate = $('option:selected', this).data('currency-rate');
        
        
        // update cache on currency change
        let cache_selectedCurr = localStorage.setItem(storagename, '{"cur":"' + selected_curr + '","sym":"' + currencySymbol + '","rate":"' + currencyRate + '"}');
    })

    if (localStorage.getItem(storagename)) {
        let cache = localStorage.getItem(storagename);
        let currency;
        if (cache != null) {

            try {
                currency = JSON.parse(cache);
            } catch (e) {
                console.error("Does not found valid JSON in localstorage");
                return false;
            }
            // change currency
            let currencySelectorBox = "#cmc_usd_conversion_box";
            $(currencySelectorBox).val(currency.cur);
            $(currencySelectorBox).trigger("change");
        }
    }
    
})
