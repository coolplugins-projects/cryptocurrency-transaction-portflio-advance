import React from 'react';
import BarChart from 'react-apexcharts'
var colorArray = ['#133258', '#159601', '#760733', '#654900', '#028346', 
		  '#465898', '#284429', '#528721', '#460537', '#534137',
		  '#935133', '#263595', '#821203', '#365023', '#168359', 
		  '#301524', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
		  '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC', 
		  '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
		  '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680', 
		  '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
		  '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3', 
		  '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'];
      class AllocationView extends React.Component {
        constructor(props) {
          super(props);
          this.state = {         
            series: [],
            options: {
              colors:colorArray,
              chart: {
                type: 'bar',
                stacked: true,
                stackType: '100%',
                toolbar:false
              },
              plotOptions: {
                bar: {
                  horizontal: true,
                  barHeight: '20%',
                  borderRadius: 10,
                },
              },
              stroke: {
                width: 1,
                colors: ['#fff']
              },
              title: {
                text: 'Allocation',
                align: 'left',
                margin: 10,
                offsetX: 0,
                offsetY: 30,
                floating: false,
                style: {
                  fontSize:  '20px',
                  fontWeight:  'bold',
                  fontFamily:  undefined,
                  color:  '#263238'
                },
              },
              
              dataLabels: {
                enabled: false,
              },
              xaxis: {
                labels: {
                    show: false
                  },
                  axisBorder:{
                      show:false
                  }
              },
              yaxis: {
                labels: {
                    show: false
                  },
                
              },
              fill: {
                opacity: 1
              
              },
              legend: {
                position: 'bottom',
                showForSingleSeries: true,
                horizontalAlign: 'left',
                offsetX: 0,
                offsetY: -30,
                formatter: function(seriesName, opts) {
                    return [seriesName, " - ", opts.w.globals.series[opts.seriesIndex] + "%"]
                }
              }
            },
            
          
          
          };
        }
        Alloction_data(alloctions){
            let total_allocation = 0;
            let alloction_array=[]
            alloctions.map(alloction =>{
                if(alloction.holdings > 0){
                    total_allocation += alloction.holdings
                }
            })
            alloctions.map(alloction =>{
                if(alloction.holdings > 0){
                    let holding_per = (alloction.holdings / total_allocation ) *100
                    alloction_array.push({
                        "name": alloction.coin_symbol,
                        "data": [this.props.formatFun(holding_per)]
                      })
                }
            })
            var barcolors = []
            for(var i =0;i<100;i++){
              let color = "#" + ((1<<24)*Math.random() | 0).toString(16)
              barcolors.push(color)
            }
            this.setState({series:alloction_array})
            
        }
        componentDidMount(){
            this.Alloction_data(this.props.holdings)
        }

        componentDidUpdate(prevProps,prevState){
            if(this.props.holdings !== prevProps.holdings){
            this.Alloction_data(this.props.holdings)
            }
        }

      

        render() {
          return (
        <React.Fragment>
            {this.state.series.length !== 0?
            <div id="cctp-allocation-view">
                <BarChart options={this.state.options} series={this.state.series} type="bar" height={200} />
            </div>:null}
        </React.Fragment>

          )
        }
    }
export default AllocationView;