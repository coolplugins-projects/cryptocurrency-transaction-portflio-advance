import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';
import Icon from '../Icon';

//Transaction List Column
const columns = [
    {
        name: 'Type',
        selector:(row)=>row.date,
        cell:(row)=> <div className="cctp-name">
                <span className="cmc_coin_symbol">{row.type}<br/>{row.date}</span>
            </div>,
        sortable: true,
    },
    {
        name: 'Price',
        selector:(row)=>row.price,
        cell:(row)=><span>{row.symbol}{row.price}</span>,
        sortable: true,
    },
    {
        name: 'Amount',
        selector: row => row.holdings,
        cell:(row)=><div><span className="cctp-title">{row.symbol}{row.holdings}</span> <span className="cctp-symbol">{row.quantity}{row.coin_symbol}</span></div>,
        sortable: true,
        conditionalCellStyles:[{
            when: row => row.type == "buy",
            classNames: ['cctp-buy'] 
        },{
            when: row => row.type == "sell",
            classNames: ['cctp-sell']  
        }
    ]
    },
    {
        name: 'Fee',
        selector: row => row.fee,
        cell:(row)=><span>{row.fee_symbol}{row.fee}</span>,
        sortable: true,
    },
    {
        name: 'Note',
        selector: row => row.note,
        cell:(row)=><span>{row.note}</span>,
    },
    {
        name: 'Actions',
        selector: row => row.action,
    },
];

//Fetch Transaction According to Asset Select
function TransactionList(props){
    let all_transaction = 0
            let table_data=[]
           let table = props.transactions.map((transaction,index)=>{
        if(transaction.coin == props.asset ){
            all_transaction += 1
            let note = transaction.note
            let fee =  transaction.fee * props.fiatRate
            let symbol = <span dangerouslySetInnerHTML={{__html:props.fiatSymbol}}/>
            if(fee == 0){
                 fee = "--"
                 symbol=""
            }
            if(note==""){
                note = "--"
            }
            let d = new Date(transaction.transaction_date);
            let data ={
                symbol:props.fiatSymbol,
                type:transaction.transaction_type,
                date:d.toLocaleString(),
                price:props.format(transaction.price * props.fiatRate),
                holdings:props.format(transaction.price * props.fiatRate*transaction.quantity),
                quantity:transaction.quantity,
                coin_symbol:props.coin_symbol,
                fee:fee,
                fee_symbol:symbol,
                note:note,
                action:<div><span name="Add Transaction" className="cctp-add-transaction" onClick={()=>props.add_trans(props.asset)}>{Icon().AddTransaction}</span>
                <span className="cctp-edit-transaction" onClick={()=>props.edit_trans(transaction.transaction_id)}>{Icon().EditIcon}</span>
                <span  className="cctp-delete-transaction" onClick={()=>props.delete_trans(transaction.transaction_id)}>{Icon().DeleteIcon}</span></div>
            } 
              table_data.push(data)
        }
})


   return(
    <div className="cctp-model-transactionlist-wrapper">
    <div className="cctp-model-transaction-content">
        <div className="cctp-transaction-model-title">Transaction List <span className="cctp-coin-name">({props.assetName})</span><span className="close-btn" onClick={()=>jQuery('.cctp-model-transactionlist-wrapper').css("display","none")}>X</span></div>
        <div className="cctp-transaction-model-form">
            {all_transaction <= 10 ?
            <div className="cctp-asset-table">
                <DataTable
                columns={columns}
                data={table_data}
                highlightOnHover='yes'
                defaultSortFieldId={1} 
                fixedHeader={true}
                fixedHeaderScrollHeight={350+"px"}
                />
                </div>
                :
                <div className="cctp-asset-table pagination">
                <DataTable
                columns={columns}
                data={table_data}
                highlightOnHover='yes'
                pagination
                defaultSortFieldId={1} 
                fixedHeader={true}
                fixedHeaderScrollHeight={350+"px"}
                />
                </div>
            }
        </div>
    </div>

</div>
   )
}

export default TransactionList