import React, { useState, useEffect } from 'react';
import axios from 'axios';
import AddComponent from './AddComponent';
import DeleteComponent from './DeleteComponent'
import Icon from '../Icon';
import DOMPurify from 'dompurify';
const url = `${appLocalizer.apiUrl}/wp-json/wp/v2/crypto_portfolio`;
function PortfolioSiderbar(props){
    let portfolio_price=0
    const [ portfolio, setPortfolio ] = useState( '' );
    const [ edit_portfolio_id, setEdit_portfolio_id ] = useState( '' );
    const [ edit_name, setName ] = useState( '' );
    const [ delete_id, setDeleteId ] = useState( '' );
    const [ add_portfolio_loader, setAddPortfolioLoader ] = useState( false );
    const [ delete_portfolio_loader, setDeletePortfolioLoader ] = useState( false );
    const [ delete_modal_state, setDeleteModalState ] = useState( false );
    const [ add_modal_state, setAddModalState ] = useState( props.add_modal );
    const [ edit_modal_state, setEditModalState ] = useState( false );

    let icons =Icon()
    //Create Portfolio
    const create_post=(value)=>{
        setAddPortfolioLoader(true)
        axios.post(url,{
            slug:value,
            title:value,
            status:'private',
            my_field:[],
            total_portfolio:0,
            portfolio_assets:[],
            portfolio_color_scheme:portfolioColor()
        },{
            headers:{
                'content-type': 'application/json',
                'X-WP-NONCE': appLocalizer.nonce
            }
        })
        .then( ( res ) => {
            setAddPortfolioLoader(false)
               
                setPortfolio("")
                props.portfolio_reset('update_portfolio')

                 props.close_modal(false)

                modal_state_fun(false)
            } )
    
        }   
    //Select Id for Edit
    const edit_select=(id,portfolio_name)=>{
        setEdit_portfolio_id(id)
        setName(portfolio_name)
        setEditModalState(true)
    }

    //Edit Portfolio
    const edit_post =(value)=>{
        setAddPortfolioLoader(true)
        axios.post(url+'/'+edit_portfolio_id,{
            slug:value,
            title:value
            // author:appLocalizer.author_id,
        },{
            headers:{
               'content-type': 'application/json',
                'X-WP-NONCE': appLocalizer.nonce
            }
        })
        .then( ( res ) => {
            setAddPortfolioLoader(false)
            props.portfolio_reset('update_portfolio')
            modal_state_fun(false)
            } )
    }

    // Delete Portgolio
    const deletePortfolio = (id) =>{
        setDeletePortfolioLoader(true)
        axios.delete(url+'/'+id,{
            headers:{
                'content-type': 'application/json',
                'X-WP-NONCE': appLocalizer.nonce
            }
        })
        .then( ( res ) => {
           props.portfolio_reset('portfolio_reset')
            setDeletePortfolioLoader(false)
            modal_state_fun(false)
            } )
    }

    const toggleButton = (value)=>{
    
        jQuery(".cctp-entity-action#cctp-portfolio-"+value).css("display","block")
        jQuery(document).click(function(){  
        jQuery(".cctp-entity-action#cctp-portfolio-"+value).css("display","none")
        })
    }

    //Random Portfolio Color
    const portfolioColor=()=>{
        let color = "#" + ((1<<24)*Math.random() | 0).toString(16)
        return color
    }

    useEffect(() => {
        setAddModalState(props.add_modal)
     },[props.add_modal])

    
    
    let portfolio_list = props.portfolios.map(portfolio => {
        let active_class=""
        let total_portfolio = 0
        if(props.select_portfolio_id == portfolio.id){
            active_class = 'current'
        }
        props.coinData.map(coin=>{
        portfolio.my_field.map(transaction=>{
                if(coin.coin_id == transaction.coin){              
                    if(transaction.transaction_type == 'buy'){
                        total_portfolio = total_portfolio + Number(transaction.quantity) * Number(coin.price)
                        }
                        else{
                            total_portfolio = total_portfolio - Number(transaction.quantity) * Number(coin.price)
                        }
                }
            })
        })
        portfolio_price = portfolio_price + Number(total_portfolio)

        return(
        <div key={portfolio.id} className={"cctp-col "+active_class}>
            <div className="cctp-icon" style={{background:portfolio.portfolio_color_scheme}}  onClick={()=>{
            props.changePortfolioFun(portfolio.id)}}>{portfolio.slug[0].toUpperCase()}</div>
            <div className="cctp-content"  onClick={()=>{
            props.changePortfolioFun(portfolio.id)}} >
                <span className="cctp-title" >{(portfolio.title.rendered.replace("Private:","")).replace("&#8211;","-")}</span>
                <span className="cctp-price"><span dangerouslySetInnerHTML={{__html: props.CoinSymbol}}/>{props.format_fun(props.FiatRate*total_portfolio)}</span>
                </div>
                <div className="cctp-portfolio-check-icon">&#10003;</div>
                <div className="cctp-entity-wrap"><div className="cctp-entity-icon"  onClick={()=>toggleButton(portfolio.id)}>&#8942;</div>
                <div className="cctp-entity-action" id={`cctp-portfolio-${portfolio.id}`}>
                <button className="edit_portfolio" onClick={()=>edit_select(portfolio.id,(portfolio.title.rendered.replace("Private:","").replace("&#8211;","-")))}>{icons.EditIcon} Edit Portfolio</button>
                <button className="delete_portfolio" onClick={()=>{setDeleteModalState(true)
                    setDeleteId(portfolio.id)}}>{icons.DeleteIcon} Delete Portfolio</button></div>
                    </div>
            
        </div>
        )
    })
    
 
    const modal_state_fun=(value)=>{
        props.close_modal(false)
        setAddModalState(value)
        setEditModalState(value)
        setDeleteModalState(value)
        setAddPortfolioLoader(value)
        setDeletePortfolioLoader(value)
    }
   
    return (
        <React.Fragment>
            <div className="cctp-portfolio-sidebar">
                <div className="cctp-row portfolio">
                    <div className="cctp-icon">A</div>
                    <div className="cctp-content">
                        <span className="cctp-title">All Portfolios</span>
                        <span className="cctp-price"><span dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(props.CoinSymbol)}}/>{props.format_fun(props.FiatRate*portfolio_price)}</span>
                    </div>
                </div>
                <div className="cctp-row">
                {portfolio_list}
                </div>
                <div className="cctp-row cctp-create-portfolio" onClick={()=>setAddModalState(true)}>
                    <div className="cctp-plusicon">+</div>
                    <div className="cctp-content">
                        <span className="cctp-title" >Create portfolio</span>
                    </div>
                </div>
            </div>

            {add_modal_state == true &&
                <AddComponent  loader={add_portfolio_loader} name={portfolio} modalClose={(value)=>modal_state_fun(value)} create_portfolio={(value)=>create_post(value)} action="Create" />
            }   
            {edit_modal_state == true &&
                <AddComponent  loader={add_portfolio_loader} name={edit_name} modalClose={(value)=>modal_state_fun(value)} create_portfolio={(value)=>edit_post(value)} action="Edit" />
            }   
            
            {delete_modal_state == true ?
                <DeleteComponent loader={delete_portfolio_loader} id={delete_id} deleteFun={(id)=>deletePortfolio(id)} component="Porfolio" modalClose={(value)=>modal_state_fun(value)} />
                :
                null
                    }
    </React.Fragment>
    )

}

export default PortfolioSiderbar