import React, { useState, useEffect } from 'react';
import Chart from 'react-apexcharts'
import axios from 'axios';



    class HistoricalChart extends React.Component {
        constructor(props) {
          super(props);
      
          this.state = {
            selection:"24_hr",
            chart_data_24h:[],
            chart_data_1y:[],
            fiatSymbol:"",
            options: {
              tooltip: {x:
                {format : 'MM/dd/yyyy  hh:mm TT'}
              },
              plotOptions: {
                area: {
                    fillTo: 'end',
                }
            },
                chart: {
                  toolbar:{
                    show:false
                  },
                  id: 'area-datetime',
                  type: 'area',
                  height: 350,
                  zoom: {
                    autoScaleYaxis: true
                  }
                },
                dataLabels: {
                    enabled: false
                  },
                  annotations: {
                    yaxis: [{
                      y: 30,
                      borderColor: '#999',
                      label: {
                        show: true,
                        text: 'Support',
                        style: {
                          color: "#fff",
                          background: '#00E396'
                        }
                      }
                    }],
                    xaxis: [{
                      borderColor: '#999',
                      yAxisIndex: 0,
                      label: {
                        show: true,
                        text: 'Rally',
                        style: {
                          color: "#fff",
                          background: '#775DD0'
                        }
                      }
                    }]
                },
                xaxis: {
                    type: 'datetime',
                    labels: {
                      datetimeUTC: false
                  }
                  },
            },

            series: [{
              name:"Balance",
                data: []
              }]
          }
        }
        componentDidMount(){
          let symbol =this.props.fiatSymbol
          let fiatRate = this.props.fiatRate
          let latest_point = [Math.round(new Date().getTime()),this.props.total_portfolio]
          let default_data = [...this.props.chartData7D]
          default_data.push(latest_point)
          this.setState({
            series:[{"data":default_data}],
            options:{yaxis: {
              labels: {
                formatter: function(value,index) {
                 let val = value*fiatRate;
                  let formatted =0
                  if (val >= 25) {
                      return formatted = symbol+parseFloat(val).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                  } else if (val >= 0.50 && val < 25) {
                      return formatted =symbol+parseFloat(val).toFixed(2)
                  } else if (val >= 0.01 && val < 0.50) {
                      return formatted =symbol+ parseFloat(val).toFixed(4);
                  } else if (val >= 0.0001 && val < 0.01) {
                      return formatted = symbol+parseFloat(val).toFixed(5);
                  } else if (val >= 0.00001 && val < 0.0001) {
                      return formatted = symbol+parseFloat(val).toFixed(6);
                  } 
                   else if (val >= 0.000001 && val < 0.00001) {
                      return formatted = symbol+parseFloat(val).toFixed(8);
                  }
                  else if(val < -25){
                      return formatted = symbol+parseFloat(val).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
                  }
                  else if(val <= -0.01){
                      return formatted = symbol+parseFloat(val).toFixed(2);
                  } 
                  else if(val <= -0.001){
                      return formatted = symbol+parseFloat(val).toFixed(3);
                  } 
                  else if(val <= -0.00001){
                      return formatted = symbol+parseFloat(val).toFixed(5);
                  } 
                  else if (val == 0) {
                      return formatted = symbol+parseFloat(val).toFixed(2);
                  } 
                   else {
                      return formatted = symbol+parseFloat(val).toFixed(8);
                  }
          
                }                    }
            }
          },
            chart_data_24h:this.props.chartData7D,
            chart_data_1y:this.props.chartData1Y,
        })
        }

        componentDidUpdate(prevProps, prevState){
        
          if(this.props.chartData7D !== prevProps.chartData7D ){
            let latest_point = [Math.round(new Date().getTime()),this.props.total_portfolio]
            let default_data = [...this.props.chartData7D]
            default_data.push(latest_point)
            let symbol =this.props.fiatSymbol
            let fiatRate = this.props.fiatRate
            this.setState({
                series:[{"data":default_data}],
                options:{yaxis: {
                  labels: {
                    formatter: function(value,index) {
                     let val = value*fiatRate;
                      let formatted =0
                      if (val >= 25) {
                          return formatted = symbol+parseFloat(val).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                      } else if (val >= 0.50 && val < 25) {
                          return formatted =symbol+parseFloat(val).toFixed(2)
                      } else if (val >= 0.01 && val < 0.50) {
                          return formatted =symbol+ parseFloat(val).toFixed(4);
                      } else if (val >= 0.0001 && val < 0.01) {
                          return formatted = symbol+parseFloat(val).toFixed(5);
                      } else if (val >= 0.00001 && val < 0.0001) {
                          return formatted = symbol+parseFloat(val).toFixed(6);
                      } 
                       else if (val >= 0.000001 && val < 0.00001) {
                          return formatted = symbol+parseFloat(val).toFixed(8);
                      }
                      else if(val < -25){
                          return formatted = symbol+parseFloat(val).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
                      }
                      else if(val <= -0.01){
                          return formatted = symbol+parseFloat(val).toFixed(2);
                      } 
                      else if(val <= -0.001){
                          return formatted = symbol+parseFloat(val).toFixed(3);
                      } 
                      else if(val <= -0.00001){
                          return formatted = symbol+parseFloat(val).toFixed(5);
                      } 
                      else if (val == 0) {
                          return formatted = symbol+parseFloat(val).toFixed(2);
                      } 
                       else {
                          return formatted = symbol+parseFloat(val).toFixed(8);
                      }
              
                    }                    }
                }
              },
                chart_data_24h:this.props.chartData7D,
                chart_data_1y:this.props.chartData1Y,
            })
        }
      }

       /**
      * Delete Startig Zero Points
      * */
      pointsUpdated(timeline){
        let final_data = []
        let points = [];
        let point_break = 0;
        let data = [...this.state.chart_data_1y]
        let latest_point = [Math.round(new Date().getTime()),this.props.total_portfolio]
        data.map(point=>{
         
          if(point[1] == 0 && point_break == 0){
            point_break = 0
          }
          else{
            point_break =1
            points.push(point)
          }
        })
        points.push(latest_point)       
        if(timeline == "7D"){
            if(points.length > 7){
              final_data =points.slice(-7)
            }
            else{
              final_data = points
            }
        }
        else if(timeline == "30D"){
            if(points.length > 30){              
              final_data =points.slice(-30)
            }
            else{
              final_data = points
            }
        }
        else if(timeline == "90D"){
            if(points.length > 90){           
              final_data =points.slice(-90)
            }
            else{
              final_data = points
            }
        }
        else if(timeline == "1Y"){ 
              final_data = points
        }
        return final_data;
      }
      
      /**
     * Update Data According to selection.
     */
        updateData(timeline){
          this.setState({
            selection: timeline
          })
          switch (timeline) {
            case '24_hr':
              this.setState({selection:"24_hr"})
              let point_24 = [...this.state.chart_data_24h]
              let latest_point = [Math.round(new Date().getTime()),this.props.total_portfolio]
              point_24.push(latest_point)
              ApexCharts.exec(
                'area-datetime',
                'updateSeries',
                [{"data":point_24 }]
              )
              break
            case '7D':
              this.setState({selection:"7D"})
              let point_7 = this.pointsUpdated("7D")
              ApexCharts.exec(
                'area-datetime',
                'updateSeries',
                [{"data":point_7}]
                )
                break
                case '30D':
                  this.setState({selection:"30D"})
                  let point_30 = this.pointsUpdated("30D")
                 
              ApexCharts.exec(
                'area-datetime',
                'updateSeries',
                [{"data":point_30}]
              )
              break
            case '90D':
              this.setState({selection:"90D"})
              let point_90 = this.pointsUpdated("90D")
              ApexCharts.exec(
                'area-datetime',
                'updateSeries',
                [{"data":point_90}]
              )
              break
            case '1Y':
              this.setState({selection:"1Y"})
              this.setState({selection:"1Y"})
              let point_365 = this.pointsUpdated("1Y")
              ApexCharts.exec(
                'area-datetime',
                'updateSeries',
                [{"data":point_365}]
              )
              break
            default:
          }
        }

        render() {
          return (
       
            <div id="cctp_chart">
            <div className="toolbar">
              <button id="24_hr" onClick={()=>this.updateData('24_hr')} className={this.state.selection == "24_hr" ? "active" : ""}>
                24H
              </button>
              &nbsp;
              <button id="7D" onClick={()=>this.updateData('7D')} className={this.state.selection == "7D" ? "active" : ""}>
                7D
              </button>
              &nbsp;
              <button id="30D" onClick={()=>this.updateData('30D')} className={this.state.selection == "30D" ? "active" : ""}>
                30D
              </button>
              &nbsp;
              <button id="90D" onClick={()=>this.updateData('90D')} className={this.state.selection == "90D" ? "active" : ""}>
                90D
              </button>
              &nbsp;
              <button id="1Y" onClick={()=>this.updateData('1Y')} className={this.state.selection == "1Y" ? "active" : ""}>
                1Y
              </button> 
            </div>
            <div id="cctp_chart-timeline">
            <Chart options={this.state.options} series={this.state.series} type="area" width={100 + "%"} height={320} />
            </div>
            </div>
         
          )
        }
      }

export default HistoricalChart