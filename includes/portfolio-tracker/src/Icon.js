const Icon = () => (
    {
    EditIcon:
    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" height="24px" width="24px" viewBox="0 0 24 24" className="sc-16r8icm-0 jZwKai" style={{width: 18+"px"},{height: 18+"px"}}><path d="M20.7604 6.51232L17.4876 3.23956C17.3341 3.08617 17.1261 3 16.9091 3C16.6921 3 16.4841 3.08617 16.3306 3.23956L14.0453 5.52476L18.4752 9.95445L20.7604 7.66924C20.9138 7.51581 21 7.30774 21 7.09078C21 6.87383 20.9138 6.66575 20.7604 6.51232Z"></path><path d="M12.8883 6.68165L4.87555 14.6942C4.78589 14.7841 4.71838 14.8936 4.67836 15.0141L3.04193 19.9233C3.00098 20.0462 2.98982 20.1771 3.00936 20.3052C3.0289 20.4333 3.07858 20.5549 3.15432 20.66C3.23006 20.7652 3.32969 20.8508 3.445 20.9099C3.56031 20.969 3.68802 20.9999 3.81759 21C3.90552 21 3.99286 20.9856 4.07615 20.9575L8.98545 19.3211C9.10594 19.2811 9.21547 19.2135 9.30537 19.1239L17.3182 11.1113L12.8883 6.68165Z"></path>
    </svg>, 
  
    DeleteIcon:
    <svg xmlns="http://www.w3.org/2000/svg" fill="none" height="24px" width="24px" viewBox="0 0 24 24" className="sc-16r8icm-0 jZwKai" style={{width: 18+"px"},{ height: 18+"px"}}><path d="M18.5455 9.54545L17.8361 19.4798C17.8067 19.8927 17.622 20.279 17.3191 20.5611C17.0162 20.8432 16.6177 21 16.2038 21H7.79618C7.3823 21 6.98378 20.8432 6.68091 20.5611C6.37803 20.279 6.1933 19.8927 6.16391 19.4798L5.45455 9.54545M3 6.27273H21M8.72727 6.27273V3.81818C8.72727 3.60119 8.81347 3.39308 8.96691 3.23964C9.12035 3.0862 9.32846 3 9.54545 3H14.4545C14.6715 3 14.8796 3.0862 15.0331 3.23964C15.1865 3.39308 15.2727 3.60119 15.2727 3.81818V6.27273" stroke="currentColor" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"></path></svg>
    ,
    ViewTransaction:
    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" height="18px" width="18px" viewBox="0 0 24 24" color="currentColor" className="sc-16r8icm-0 TyMwO"><path d="M6 16H20M20 16L17 19M20 16L17 13" stroke="currentColor" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"></path><path d="M18 8H4M4 8L7 11M4 8L7 5" stroke="currentColor" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"></path></svg>,

    AddTransaction:
    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" height="16px" width="16" viewBox="0 0 24 24" color="neutral4" className="sc-16r8icm-0 iRRzNg"><path d="M20.4852 11.9999H11.9999M11.9999 11.9999H3.51465M11.9999 11.9999V20.4852M11.9999 11.9999V3.51465" stroke="currentColor" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"></path></svg>
}
    ); 
    export default Icon;