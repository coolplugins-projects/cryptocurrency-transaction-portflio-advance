import React from 'react';
import axios from 'axios';
import PortfolioSiderbar from './components/PortfolioSiderbar';
import PortfolioData from './components/PortfolioData';


const url = `${appLocalizer.apiUrl}/wp-json/wp/v2/crypto_portfolio`;
class App extends React.Component {
    constructor() {
	    super( ...arguments );
        
		this.state = {
            portfolio: "",
            portfolio_list:[],
            select_portfolio:"",
            coinData:[],
            coinFiatRate:1,
            selectFiatCurrency:"USD",
            coinSymbol:"$",
            loaderState:true,
            add_modal:false
		};
        
	}
    
    componentDidMount(){
        let fiat = localStorage.getItem(`cmc-selected-currency-${appLocalizer.apiUrl}`)
        if(fiat != null ){
            let parse_data  = JSON.parse(fiat)
            this.setState({coinSymbol:parse_data.sym,selectFiatCurrency:parse_data.cur,coinFiatRate:Number(parse_data.rate)})
        }
        this.fetch_portfolio_posts("portfolio_reset")
        this.fetchCoinData()
        setInterval(()=>{              
            this.fetchCoinData()
        },600000
        )
        axios.post(`${appLocalizer.apiUrl}/wp-json/coin-market-cap/v1/cctp_save_fiat_currency`,{
            'portfolio_fiat_currency':fiat
        },{
            headers:{
                'content-type': 'application/json',
                'X-WP-NONCE': appLocalizer.nonce
            }
        })
      
    }
    
    // Fetch Coin Data From Rest Api
    fetchCoinData = () => {         
        axios.get( `${appLocalizer.apiUrl}/wp-json/coin-market-cap/v1/cmc_get_top_100_coins` )
        .then( ( res ) => {
           this.setState({coinData:res.data.data})
        })
    } 

    // Fetch Portfolio According to Author
    fetch_portfolio_posts(status){
        axios.get( url+'?author='+appLocalizer.author_id+'&&status=private' ,{
            headers:{
                'content-type': 'application/json',
                'X-WP-NONCE': appLocalizer.nonce
            }
        })
        .then( ( res ) => {
            this.setState({portfolio_list:res.data})
            let portfolio_select = localStorage.getItem(`cctp-selected-portfolio`)
            if(status == 'portfolio_reset' ){
                if(res.data.length != 0){
                if(portfolio_select !== null){
                    this.setState({select_portfolio:portfolio_select})
                }
                else{
                this.setState({select_portfolio:res.data[0].id})
                }
            }
            else{
                localStorage.removeItem(`cctp-selected-portfolio`)
            }
                this.setState({loaderState:false})
            }
            if(status == 'update_portfolio' && res.data.length == 1){
                this.setState({select_portfolio:res.data[0].id})
            }
        } )
    }

    createPortfolio(value){
       this.setState({add_modal:value})
    }
    
    // Change Portfolio Selection
    changePortfolio(value){
        localStorage.setItem(`cctp-selected-portfolio`,value)
        this.setState({select_portfolio:value})
    }

    //Update Portfolio
    portfolioPriceUpdate(){
        this.fetch_portfolio_posts('update_portfolio')  
    }

    // Formatting Function
    formatFunction(value){
        let formatted =0
        if (value >= 25) {
            return formatted = parseFloat(value).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        } else if (value >= 0.50 && value < 25) {
            return formatted =parseFloat(value).toFixed(2)
        } else if (value >= 0.01 && value < 0.50) {
            return formatted = parseFloat(value).toFixed(4);
        } else if (value >= 0.0001 && value < 0.01) {
            return formatted = parseFloat(value).toFixed(5);
        } else if (value >= 0.00001 && value < 0.0001) {
            return formatted = parseFloat(value).toFixed(6);
        } 
         else if (value >= 0.000001 && value < 0.00001) {
            return formatted = parseFloat(value).toFixed(8);
        }
        else if(value < -25){
            return formatted = parseFloat(value).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
        }
        else if(value <= -0.01){
            return formatted = parseFloat(value).toFixed(2);
        } 
        else if(value <= -0.001){
            return formatted = parseFloat(value).toFixed(3);
        } 
        else if(value <= -0.00001){
            return formatted = parseFloat(value).toFixed(5);
        } 
        else if (value == 0) {
            return formatted = parseFloat(value).toFixed(2);
        } 
         else {
            return formatted = parseFloat(value).toFixed(8);
        }

    }
    render(){
        return(
            <React.Fragment>
                { this.state.loaderState == true ?
                <div className="ph-item">
                        <div className="ph-col-12">
                            <div className="ph-row">
                                <div className="ph-col-6 big"></div>
                                <div className="ph-col-4  big"></div>
                                <div className="ph-col-2 big"></div>
                                <div className="ph-col-4"></div>
                                <div className="ph-col-8 "></div>
                                <div className="ph-col-6"></div>
                                <div className="ph-col-6 "></div>
                                <div className="ph-col-12"></div>
                            </div>
                        </div>
                    </div>
                    :
                <div className="cctp-portfolio-wrapper">
                    <PortfolioSiderbar portfolios={this.state.portfolio_list}   select_portfolio_id = {this.state.select_portfolio} changePortfolioFun={(value)=>this.changePortfolio(value)}  portfolio_reset={(value)=>this.fetch_portfolio_posts(value)} coinData={this.state.coinData} FiatRate={this.state.coinFiatRate} CoinSymbol={this.state.coinSymbol} format_fun={(value)=>this.formatFunction(value)} add_modal={this.state.add_modal} close_modal={(value)=>this.createPortfolio(value)} />
                    {this.state.portfolio_list.length != 0 ?
                    <PortfolioData select_portfolio_id = {this.state.select_portfolio} transaction_total ={()=>this.portfolioPriceUpdate()} coinData={this.state.coinData} FiatRate={this.state.coinFiatRate} CoinSymbol={this.state.coinSymbol} format_fun={(value)=>this.formatFunction(value)}/>
                       :
                            <div  className="cctp-empty-portfolio">Please create Portfolio
                            <div className="cctp-btn">
                                <button className="addnew-btn" onClick={()=>this.createPortfolio(true) }>+ Add New </button>
                            </div>    
                            </div> 
                     
                }
                </div>
    }
            </React.Fragment>
        )
    }
}
export default App;